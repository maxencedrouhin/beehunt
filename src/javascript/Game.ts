import {Bee} from "./model/Bee";
import {Random} from "./service/utils/Random";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const $ = require('jquery');

export class BeeHuntGame {

    private static readonly beesContainerSelector = "#beehive";
    private static readonly beesSelector = `${BeeHuntGame.beesContainerSelector} .bee`;
    private static readonly resetButton = "#resetGameButton";
    private static readonly hitButton = "#hitRandomBeeButton";
    private static readonly clickCountSelector = '#clickCount';

    start() {
        this.createBees();
        this.handleButtons();
    }

    /**
     * Get bee html inner display element
     * @param type
     * @param hp
     * @returns {string}
     */
    private getBeeHtmlDataContent(type: string, hp: number) {
        return `<span>${type} | HP ${hp}</span>`;
    }

    /**
     * Get a bee html element
     * @param type
     * @param hp
     * @param loss
     * @returns {string}
     */
    private getBeeHtml(type: string, hp: number, loss: number) {
        return `<div class="bee" data-type="${type}" data-hp="${hp}" data-loss="${loss}">${this.getBeeHtmlDataContent(type, hp)}</div>`;
    }

    /**
     * Append bees to DOM
     */
    private createBees() {
        let beesConfig = [new Bee('queen', 100, 1, 15),
            new Bee('worker', 50, 5, 20),
            new Bee('scout', 30, 8, 15)
        ];
        let intMax = beesConfig.length;
        while (beesConfig.length) {
            const beeIdx = Random.getRandomInt(intMax);
            const bee = beesConfig[beeIdx] as Bee;
            $(BeeHuntGame.beesContainerSelector).append(this.getBeeHtml(bee.type, bee.hp, bee.loss));
            const newCount = bee.count - 1;
            bee.count = newCount;
            if (0 === bee.count) {
                intMax--
                delete beesConfig[beeIdx];
                //reset array indexes
                beesConfig = beesConfig.filter(() => {
                    return true;
                })
            }
        }
    }

    /**
     * Get a random bee from living bees
     * @returns {*|jQuery|HTMLElement}
     */
    private getRandomBee() {
        const remainingBees = this.getLivingBees();
        const remainingBeesCount = remainingBees.length;
        const targetBeeIdx = Random.getRandomInt(remainingBeesCount);
        return $(remainingBees[targetBeeIdx]);
    }

    /**
     *
     * Returns bees that still have hit points
     * @returns {*|jQuery}
     */
    private getLivingBees() {
        return $(BeeHuntGame.beesSelector).filter((idx: number, bee: string) => {
            return $(bee).data('hp') > 0;
        });
    }

    /**
     * Are any bee with hit points left ?
     * @returns {boolean}
     */
    private isAnyBeeRemaining() {
        return this.getLivingBees().length > 0;
    }

    /**
     * Update bee html element in dom to reflect its state
     * @param bee
     * @param newHp
     */
    /* eslint-disable  @typescript-eslint/no-explicit-any */
    private updateBeeHp(bee: any, newHp: number) {
        bee.data('hp', newHp);
        bee.html(this.getBeeHtmlDataContent(bee.data('type'), newHp));
        bee.addClass('targeted');
    }

    /**
     * Decrease hp from a random alive bee if any
     */
    private hitRandomBee() {
        const beeFound = this.getRandomBee();
        const newHp = beeFound.data('hp') - beeFound.data('loss');
        this.updateBeeHp(beeFound, newHp > 0 ? newHp : 0);
    }

    private handleEnablingOrDisablingButtons() {
        $(BeeHuntGame.resetButton).prop('disabled', this.isAnyBeeRemaining());
        $(BeeHuntGame.hitButton).prop('disabled', false === this.isAnyBeeRemaining());
    }

    private handleButtons() {
        $(BeeHuntGame.hitButton).bind('click', () => {
            $(`${BeeHuntGame.beesSelector}`).removeClass('targeted');
            this.hitRandomBee();
            this.handleEnablingOrDisablingButtons();
            $(BeeHuntGame.clickCountSelector).data('count', $(BeeHuntGame.clickCountSelector).data('count') + 1);
            $(BeeHuntGame.clickCountSelector).html($(BeeHuntGame.clickCountSelector).data('count'));
        });

        $(BeeHuntGame.resetButton).on('click', () => {
            $(BeeHuntGame.clickCountSelector).html(0);
            $(BeeHuntGame.clickCountSelector).data('count', 0);
            $(BeeHuntGame.beesContainerSelector).html('');
            this.createBees();
            this.handleEnablingOrDisablingButtons();
        });
    }
}
