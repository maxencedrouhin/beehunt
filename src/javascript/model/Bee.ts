export class Bee {
    private _type: string;
    private _hp: number
    private _count: number;
    private _loss: number;

    constructor(type: string, hp: number, count: number, loss: number) {
        this._type = type;
        this._hp = hp;
        this._count = count;
        this._loss = loss;
        this._count = count;
    }

    get type(): string {
        return this._type;
    }

    get hp(): number {
        return this._hp;
    }

    get count(): number {
        return this._count;
    }

    get loss(): number {
        return this._loss;
    }

    set count(value: number) {
        this._count = value;
    }
}