export class Random {
    /**
     *
     * Get a pseudo random number beetween 0 and max value specified
     * @param max
     * @returns {number}
     */
    static getRandomInt(max: number) {
        return Math.floor(Math.random() * max);
    }
}