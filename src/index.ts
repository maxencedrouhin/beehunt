const $ = require('jquery');
import {BeeHuntGame} from "./javascript/Game";
import './stylesheets/bees/bee.scss';
import './stylesheets/bees/layout.scss'

$(() => {
    const game = new BeeHuntGame();
    game.start();
})
