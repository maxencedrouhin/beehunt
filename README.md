# Bee hunt game

## Requirements
-----------------

ES6 compliant web browser.

## How to play
-----------------

In order to play the game, just open the ./dist/index.html file in your browser. 

It should look like this at start:
![alt text](./src/img/readme/beehunt_preview.png)
Enjoy

## Development
-----------------

With docker: 
  
    Install dependencies:

    docker run -ti -u $UID:$UID  -p 8080:8080 -v "$(pwd)":/usr/src/app node:21 sh -c "cd /usr/src/app && npm i"


    Run development server:

     docker run -ti -u $UID:$UID  -p 8080:8080 -v "$(pwd)":/usr/src/app node:21 sh -c "cd /usr/src/app && npx webpack serve --host 0.0.0.0"

Then you should be able to access the app at http://localhost:8080/

Without docker:
    
    you will need node 21, then :

    - npm i
    - npx webpack serve

## Build
-----------------
With docker, in order to build:

    docker run -ti -u $UID:$UID -v "$(pwd)":/usr/src/app -p 8080:8080 node:21 sh -c "cd /usr/src/app && npm run build"

Without docker you will need node 21:

    npm run build
  
## Goal
-------

This test aims at creating an application that performs the following:
- produce a web page with an interface to play the game (UI design is not expected or necessary)
- a button should appear in order to "hit" a random bee
- the game must follow the rules described below

N.B: The solution should run locally (please provide a README.md). You don’t need to setup any web server or host your code online


## Specifications
-----------------

There are 3 different types of bees:

- The Queen
    - has 100 hit points
    - when the Queen is hit, then 15 hit points should be deduced from her lifespan
    - when the Queen is running out of hit points, all the other bees should automatically be out of hit points
    - there is only 1 Queen in the game

- The Worker
    - each has 50 hit points
    - when a worker is hit, he loses 20 hit points
    - there are 5 Workers at start

- The Scout
    - each has 30 hit points
    - when a scout is hit, he loses 15 hit points
    - there are 8 Scouts at start


## Gameplay
-----------

Should be visible on the UI:

	- the list of bees associated with their role (Queen, Worker, Scout) and remaining hit points
	- a clickable "hit" button

When the button is clicked:

	- a random bee should be selected 
	- the correct damages should be deduced from its lifespan

Please note that:
- if an bee is running out of hit points, then it cannot be randomly selected again
- when all bees are running out of hit points, then the game must be able to reset itself for another round